import csv
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
df = pd.read_csv('gtd.csv',sep=',',encoding='ISO-8859-1')

print("----------init--------")
print(df.head())
print(df.tail())
print("----------define modle--------")
df = df.loc[:,["iyear","imonth","country","region","attacktype1","targtype1","weaptype1","natlty1"]]
df = df.dropna(axis=0,how='any')
print(df.head())
print(df.tail())
print("----------split train and test--------")
train,test = train_test_split(df,test_size=0.2)

train.to_csv("to_gtd_train.csv",index=False,sep=',')
test.to_csv("to_gtd_test.csv",index=False,sep=',')
